<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/book', name: 'book_')]
class BookController extends AbstractController
{
    #[Route('/', name: 'list')]
    public function list(BookRepository $bookRepository): Response
    {
        $listBooks = $bookRepository->findAll();
        return $this->render('book/list.html.twig', [
            'listBooks' => $listBooks,
        ]);
    }

    #[Route('/add', name: 'add')]
   public function add(BookRepository $bookRepository): Response
    //public function add(): Response
    {
        //$em = $this->getDoctrine()->getManager();
        $book = new Book();
        $book->setAuthor("Stephen King 3")
            ->setEditeur("Gallimard")
            ->setPages(300)
            ->setTitle("The Mist");


        $bookRepository->add($book,true);
        echo "enregistrement effectué";
        die();
        /*return $this->render('book/add.html.twig', [
            'controller_name' => 'BookController',
        ]);*/
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete($id,BookRepository $bookRepository): Response
    {
        $book = $bookRepository->find($id);
        $bookRepository->remove($book,true);
        echo "suppression effectuée";
        die();
        /*return $this->render('book/add.html.twig', [
            'controller_name' => 'BookController',
        ]);*/
    }
}
