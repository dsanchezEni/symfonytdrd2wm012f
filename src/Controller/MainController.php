<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/mail', name: 'email')]
    public function sendMail(MailerInterface $mailer) : Response
    {
       $mail = (new Email())
           ->from('expediteur@demo.test')
           ->to('destinataire@demo.test')
           ->subject('Mon beau sujet')
           ->html('<p>Ceci est un message en HTML</p>')
           ;
       $mailer->send($mail);
       return $this->redirectToRoute("home");
    }


    #[Route('/main', name: 'app_main')]
    public function index(): Response
    {
        //Déclaration de mes variables que je vais passer à ma vue.
        $productCount = 222;
        $username = "Denis";
        $controller_name="MainController";

        $attaqueHTML="<h1>ceci est une attaque HTML</h1>";
        //$attaqueJS="<script>document.location.href='http://eni-ecole.fr';</script>";
        $attaqueJS="<script>alert('toto');</script>";
        /*return $this->render('main/list.html.twig', [
            'controller_name' => 'MainController',
            'productCount' => $productCount,
            'username' => $username,
        ]);*/

        //Version plus compacte avec compact()
        return $this->render('main/list.html.twig',
            compact("productCount","username","controller_name","attaqueHTML","attaqueJS"));
    }

    /**
     * Affiche la page d'accueil.
     * @return Response
     */
    #[Route('/', name: 'home')]
    //Route en version avant PHP 8.
    //@Route("/",name="home")
    public function home(): Response
    {
        return $this->render('main/home.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
    /**
     * Affiche une page de test.
     * @return Response
     */
    #[Route('/test', name: 'test')]
    public function test(): Response
    {
        return $this->render('main/test.html.twig');
    }
}
