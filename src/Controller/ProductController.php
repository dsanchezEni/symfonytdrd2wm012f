<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/product', name: 'product_')]
class ProductController extends AbstractController
{
    #[Route('/', name: 'list')]
    public function list(ProductRepository $productRepository): Response
    {
        //récuprération de tous mes produits
        $products = $productRepository->findAll();

        return $this->render('product/list.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route('/detail/{id}', name: 'detail')]
    public function detail($id,ProductRepository $productRepository): Response
    {
        //récupération du produit
        $product = $productRepository->find($id);

        return $this->render('product/detail.html.twig', [
            'product' => $product,
        ]);
    }


    #[Route('/supprimer/{id}', name: 'supprimer')]
    public function remove($id,ProductRepository $productRepository): Response
    {
        //récupération du produit
        $product = $productRepository->find($id);
        $productRepository->remove($product,true);


        return $this->redirectToRoute("product_list");
    }


    #[Route('/add', name: 'add')]
    public function add(Request $request, ProductRepository $productRepository): Response
    {
        $product = new Product();
        $product->setDateCreated(new \DateTime());
        //$product->setName("produit de ouf");
        //Permet de créer le formulaire et d'associer la création de formulaire à la classe associée à ce formulaire.
        $productForm = $this->createForm(ProductType::class, $product);

        // associe les paramètres de la requête à l'instance de product
        $productForm->handleRequest($request);

        //récupération de champs non mappé
        dump($productForm->get("description")->getData());
        //dump($request->request->get("description"));

        //si le formulaire est soumis
//        if($productForm->isSubmitted() && $productForm->isValid()){
//            //je l'enregistre
//            $productRepository->add($product, true);
//            // j'ajoute du feedback utilisateur
//            $this->addFlash("success", "Le produit a été enregitré !");
//
//            //je redirige sur la listes des produits
//            return $this->redirectToRoute("product_list");
//        }

        //!!!!!!Penser à faire appel à la création de la vue
        return $this->render('product/add.html.twig', [
            'productForm' => $productForm->createView(),
        ]);
    }

    #[Route('/testAddProductWithComment', name: 'testAddProductWithComment')]
    public function saveProductWithComment(EntityManagerInterface $entityManager){
        $product = new Product();
        $product->setName("Cours Symfony");
        $product->setPrice(10);
        $product->setDateCreated(new \DateTime());

        $comment = new Comment();
        $comment->setDescription("Super j'adore Symfony !");
        $comment->setAuteur("Fayot");

        //Associé ce commentaire au produit !
        $product->addComment($comment);
        //$entityManager->persist($product);
        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->redirectToRoute('product_list');
    }
}
