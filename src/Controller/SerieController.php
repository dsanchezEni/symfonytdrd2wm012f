<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/serie', name: 'serie_')]
class SerieController extends AbstractController
{
    private $listSerie = ["Arnold & Willy","Mc Gyver","L'agence tout risque"];
    #[Route('/', name: 'list')]
    public function list(): Response
    {
        return $this->render('serie/list.html.twig', [
            'controller_name' => 'SerieController',
        ]);
    }

    #[Route('/detail/{id}', name: 'detail')]
    public function detail($id): Response
    {
        return $this->render('serie/detail.html.twig',[
            'id' => $id
        ]);
    }

    #[Route('/creer', name: 'creer')]
    public function creer(): Response
    {
        $serie = "Notre belle famille";
        dump($this->listSerie);
        return $this->render('serie/creer.html.twig',[
            'serie' => $serie,
            'listSerie' => $this->listSerie,
        ]);
    }
}
