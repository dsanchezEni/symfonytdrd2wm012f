<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

            $product = new Product();
            $product->setName("Cours JAVA")->setPrice(100)->setDateCreated(new \DateTime());

            $comment = new Comment();
            $comment->setDescription("JAVA c'est lourd !");
            $comment->setAuteur("Le raleur");

            $comment2 = new Comment();
            $comment2->setDescription(".NET C'est pour les pros microsoft !");
            $comment2->setAuteur("Le raleur");

            $product->addComment($comment);
            $product->addComment($comment2);
            $manager->persist($comment);
            $manager->persist($comment2);

            $manager->persist($product);

            $manager->flush();
    }
}
