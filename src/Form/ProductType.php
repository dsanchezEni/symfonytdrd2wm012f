<?php

namespace App\Form;

use App\Entity\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du produit ',
                'attr' => [
                    'placeholder' => 'Nom du produit'
                ],
                'required' => false

            ])
            ->add('price', TextType::class, [
                'required' => false
            ])

            //ajout d'un champs qui ne fait pas partie de l'entité
            ->add('description', TextareaType::class, [
                'label' => "Description du produit",
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => "Le champ machin"
                    ])
                ]
            ])







        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
